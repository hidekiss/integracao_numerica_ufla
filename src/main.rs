#[macro_use]
extern crate text_io;

use std::io::Write;
use std::thread;
use std::time::Instant;

fn trapezios_threads(
    limit_a: f64,
    limit_b: f64,
    thread_numb: u64,
    function: fn(f64) -> f64,
) -> f64 {
    let threads = (0..thread_numb)
        .map(|t| {
            let h: f64 = (limit_b - limit_a) / thread_numb as f64;
            thread::spawn(move || {
                // criação das threads
                function(limit_a + h * (t as f64))
            })
        })
        .collect::<Vec<_>>();
    (((limit_b - limit_a) / thread_numb as f64) / 2.0)
        * ((function(limit_a) + function(limit_b))
            + 2.0
                * threads
                    .into_iter()
                    .map(|t| t.join())
                    .collect::<Result<Vec<_>, _>>()
                    .unwrap()
                    .iter()
                    .sum::<f64>())
}

fn trapezios_linear(limit_a: f64, limit_b: f64, iterations: u64, function: fn(f64) -> f64) -> f64 {
    (((limit_b - limit_a) / iterations as f64) / 2.0)
        * ((function(limit_a) + function(limit_b))
            + 2.0
                * (1..iterations)
                    .map(|i| {
                        function(limit_a + i as f64 * ((limit_b - limit_a) / iterations as f64))
                    })
                    .sum::<f64>())
}

fn main() {
    // test functions
    let function = |x: f64| 1.0 / (x * x);

    // input variables for the integration
    let (point_a, point_b, iterations): (f64, f64, u64);
    print!("Put your limit a: ");
    std::io::stdout().flush().unwrap();
    point_a = read!();
    print!("and your limit b: ");
    std::io::stdout().flush().unwrap();
    point_b = read!();
    print!("How many iterations? ");
    std::io::stdout().flush().unwrap();
    iterations = read!();

    // threads test
    let now_threads = Instant::now();
    let integral_threads = trapezios_threads(point_a, point_b, iterations, function);
    println!(
        "The integral value with threads: {:.10}",
        integral_threads
    );
    println!(
        "Runnig with: {} threads\nIn time: {:.5} seconds.\nWith error: {:.10}",
        iterations,
        now_threads.elapsed().as_secs_f64(),
        0.75f64 - integral_threads
    );

    // sequential test
    let now_linerar = Instant::now();
    let integral_iteradores = trapezios_linear(point_a, point_b, iterations, function);
    println!(
        "The integral value with iterators: {:.10}",
        integral_iteradores
    );
    println!(
        "Runnig with: {} iterations \nIn time: {:.5} seconds.\nWith error: {:.10}",
        iterations,
        now_linerar.elapsed().as_secs_f64(),
        (0.75f64 - integral_iteradores)
    );
}
